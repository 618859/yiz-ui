import Vue from 'vue'
import App from './App.vue'
// import router from './router'
import store from './store'

Vue.config.productionTip = false

// 组件
// import Card from './components/Card'
// import './components/Card/src/index.css'

// import Select from './components/Select'
// import './components/Select/src/index.css'
// Vue.use(Select)

// import './components/index.css'
// import YizUI from './components/index.js'
// Vue.use(YizUI)

// 全局引入
import YizUI from 'yiz-ui'
Vue.use(YizUI)


new Vue({
  // router,
  store,
  render: h => h(App)
}).$mount('#app')
