// 集中在这里注册

import Card from './card/src/main.vue'
import MyInput from './my-input/src/main.vue'
import yizTitle from './yiz-title/src/main.vue'
// console.log('card', Card);
const components = [Card, MyInput, yizTitle]
function install(Vue){
  components.forEach(component => {
    // 全局注册组件
    Vue.component(component.name, component)
  })
}

// 将注册函数导出去
export default { install }