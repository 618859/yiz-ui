const path = require('path');
// const VueLoaderPlugin = require('vue-loader/lib/plugin');
const { VueLoaderPlugin } = require("vue-loader");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
  mode: 'development',
  // entry: './src/components/index.js',
  entry: {
    // 把组件一个个写到这里才能正常打包喔
    card: './src/components/card/index.js',
    myInput: './src/components/my-input/index.js',
    yizTitle: './src/components/yiz-title/index.js',
    index: './src/components/index.js',
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].umd.js',
    library: 'YizUI',
    libraryTarget: 'umd',
  },
  // resolve: {
  //   alias: {
  //     '@': path.resolve(__dirname, 'src'),
  //   },
  // },
  plugins: [
    // 先删除原来的文件，再打包
    new CleanWebpackPlugin(),
    new VueLoaderPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'vue-style-loader', 'css-loader']
      },
      {
        test: /\.vue$/,
        use: [
          {
            loader: 'vue-loader'
          }
        ]
      },
    ]
  }
}